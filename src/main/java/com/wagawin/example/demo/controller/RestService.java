package com.wagawin.example.demo.controller;

import com.wagawin.example.demo.model.Child;
import com.wagawin.example.demo.model.House;
import com.wagawin.example.demo.model.ParentSummary;
import com.wagawin.example.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by sajjadpervaiz on 11/01/2018.
 *
 * Todo: Add flowcontrol
 * Todo: For high volume of requests, to avoid request timeouts, flowcontroll
 * Todo: Flowcontrol can manage the instances of application based on incoming request
 * 
 */
@RestController
public class RestService
{
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private HouseRepository houseRepository;

    @Autowired
    private ChildRepository childRepository;

    @Autowired
    private ParentSummaryRepository summaryRepository;

    @Autowired
    private ChildColorSpecifications specifications;

    @GetMapping("/house")
    public ResponseEntity<House> findHouseofPersonByName(@RequestParam("id") Long id)
    {
        House house = houseRepository.findOne(id);
        if (house == null)
        {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(house);
    }

    @GetMapping("/house/all")
    public ResponseEntity<List<House>> getHouse()
    {
        List<House> house = houseRepository.findHouseofPerson();
        if (house == null)
        {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<List<House>>(house, HttpStatus.OK);
    }

    @GetMapping("/child/info")
    public ResponseEntity<List<Object[]>> getChildInfo(@RequestParam("id") Long id)
    {
        List<Object[]> childInfo = childRepository.findChildInfo(id);
        if (childInfo == null)
        {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<List<Object[]>>(childInfo, HttpStatus.OK);
    }

    @GetMapping("/color")
    public ResponseEntity<List<Child>> getColor(@RequestParam("id") Long id)
    {
        List<Child> childInfo = childRepository.findAll(specifications.withDynamicQuery(id));
        if (childInfo == null)
        {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<List<Child>>(childInfo, HttpStatus.OK);
    }

    @GetMapping("/persons/children")
    public ResponseEntity<List<ParentSummary>> getPersonChildrenSummary()
    {
        List<ParentSummary> summary = summaryRepository.findAll();

        if (summary == null)
        {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<List<ParentSummary>>(summary, HttpStatus.OK);
    }
}
