package com.wagawin.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parentSummary")
public class ParentSummary
{
    //TODO: Bad approach, keep it for now since we have only one record always
    @Id
    @Column(name = "id")
    private Long id = 1L;

    @Column(name = "amountOfPersons")
    private Long amountOfPersons;
    @Column(name = "amountOfChildren")
    private Long amountOfChildren;

    public ParentSummary()
    {
    }

    public ParentSummary(Long amountOfPersons, Long amountOfChildren)
    {
        this.amountOfPersons = amountOfPersons;
        this.amountOfChildren = amountOfChildren;
    }

    public Long getAmountOfPersons()
    {
        return amountOfPersons;
    }

    public void setAmountOfPersons(Long amountOfPersons)
    {
        this.amountOfPersons = amountOfPersons;
    }

    public Long getAmountOfChildren()
    {
        return amountOfChildren;
    }

    public void setAmountOfChildren(Long amountOfChildren)
    {
        this.amountOfChildren = amountOfChildren;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
