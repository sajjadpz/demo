package com.wagawin.example.demo.model;

import javax.persistence.*;

@Entity
@DiscriminatorValue("Daughter")
@Table(name = "daughter")
public class Daughter extends Child
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "hairColor")
    private String hairColor = "Brown";

    public Daughter()
    {
    }

    public Daughter(String name, Integer age)
    {
        super(name, age);
    }

    public String getHairColor()
    {
        return hairColor;
    }

    public void setHairColor(String hairColor)
    {
        this.hairColor = hairColor;
    }
}
