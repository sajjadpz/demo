package com.wagawin.example.demo.model;

import org.springframework.stereotype.Component;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Created by sajjadpervaiz on 15/01/2018.
 * Used by Helper class that is building Criteria API
 */

@StaticMetamodel(Child.class)
public class Child_
{
    public static volatile SingularAttribute<Child, Long> id;
    public static volatile SingularAttribute<Child, String> name;
    public static volatile SingularAttribute<Child, Integer> age;
    public static volatile SetAttribute<Child, Meal> meals;
}
