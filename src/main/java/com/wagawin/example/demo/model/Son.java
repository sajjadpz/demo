package com.wagawin.example.demo.model;

import javax.persistence.*;

@Entity(name = "son")
@DiscriminatorValue("Son")
@Table(name = "son")
public class Son extends Child
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "bicycleColor")
    private String bicycleColor = "Blue";

    public Son()
    {
    }

    public Son(String name, Integer age)
    {
        super(name, age);
    }

    public String getBicycleColor()
    {
        return bicycleColor;
    }

    public void setBicycleColor(String bicycleColor)
    {
        this.bicycleColor = bicycleColor;
    }
}
