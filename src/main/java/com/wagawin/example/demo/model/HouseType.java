package com.wagawin.example.demo.model;

/**
 * Created by sajjadpervaiz on 11/01/2018.
 */
public enum HouseType
{
    FLAT,
    HOUSE,
    ESTATE
}
