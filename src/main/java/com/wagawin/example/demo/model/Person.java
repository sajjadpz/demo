package com.wagawin.example.demo.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "persons")
public class Person
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "person_name")
    private String person_name;

    @Column(name = "age")
    private Integer age;

    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "person")
    private House house;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "person")
    private Set<Child> children = new HashSet<>();

    public Person()
    {
    }

    public Person(String name, Integer age)
    {
        this.person_name = name;
        this.age = age;
    }

    public String getPerson_name()
    {
        return person_name;
    }

    public void setPerson_name(String person_name)
    {
        this.person_name = person_name;
    }

    public Integer getAge()
    {
        return age;
    }

    public void setAge(Integer age)
    {
        this.age = age;
    }

    public House getHouse()
    {
        return house;
    }

    public void setHouse(House house)
    {
        this.house = house;
    }

    public Set<Child> getChildren()
    {
        return children;
    }

    public void setChildren(Set<Child> children)
    {
        this.children = children;
    }
}
