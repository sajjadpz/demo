package com.wagawin.example.demo;

import com.wagawin.example.demo.model.*;
import com.wagawin.example.demo.repository.ChildRepository;
import com.wagawin.example.demo.repository.HouseRepository;
import com.wagawin.example.demo.repository.MealRepository;
import com.wagawin.example.demo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Calendar;

@SpringBootApplication
@EnableScheduling
public class DemoApplication implements CommandLineRunner
{
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private HouseRepository houseRepository;

    @Autowired
    private MealRepository mealRepository;

    @Autowired
    private ChildRepository childRepository;

    public static void main(String[] args)
    {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception
    {

        //Clean Up
        /*houseRepository.deleteAllInBatch();
        mealRepository.deleteAllInBatch();
        childRepository.deleteAllInBatch();
        personRepository.deleteAllInBatch();*/

        Person person1 = new Person("Jack", 29);
        Person person2 = new Person("Judy", 33);

        House house = new House("Planet X", "X000X", HouseType.HOUSE);
        House house2 = new House("Earth", "000999", HouseType.FLAT);

        person1.setHouse(house);
        house.setPerson(person1);

        person2.setHouse(house2);
        house2.setPerson(person2);



        /*Child child1 = new Child("David", 12);
        child1.setPerson(person1);
        Child child2 = new Child("Sara", 8);
        child2.setPerson(person1);*/

        Child child1 = new Son("Ross",5);
        child1.setPerson(person1);
        Child child2 = new Daughter("Rachel",2);
        child2.setPerson(person1);

        person1.getChildren().add(child1);
        person1.getChildren().add(child2);

        Calendar pizzaInvented = Calendar.getInstance();
        pizzaInvented.set(1900, 8, 2);

        Calendar pastaInvented = Calendar.getInstance();
        pizzaInvented.set(1901, 8, 2);

        Meal pizza = new Meal("Pizza", pizzaInvented.getTime());
        pizza.setChild(child1);
        Meal pasta = new Meal("Pasta", pizzaInvented.getTime());
        pasta.setChild(child2);

        child1.getMeals().add(pizza);
        child2.getMeals().add(pasta);

        personRepository.save(person1);
        personRepository.save(person2);
    }
}
