package com.wagawin.example.demo.scheduler;

import com.wagawin.example.demo.model.ParentSummary;
import com.wagawin.example.demo.repository.ChildRepository;
import com.wagawin.example.demo.repository.ParentSummaryRepository;
import com.wagawin.example.demo.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduleSummary
{
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ParentSummaryRepository summaryRepository;

    @Autowired
    private ChildRepository childRepository;


    /**
     * Runs every 15 minutes
     */
    @Scheduled(fixedDelay = 900000)
    public void generateSummary()
    {
        System.out.println("Running scheduled job...");

        Long numberOfParents = personRepository.count();
        Long numberOfChildren = childRepository.count();

        ParentSummary ps = new ParentSummary(numberOfParents, numberOfChildren);

        if (numberOfParents != null && numberOfChildren != null)
        {
            if (summaryRepository.findOne(1L) != null)
                summaryRepository.updateSummary(numberOfChildren, numberOfParents);
            else
                summaryRepository.save(ps);
        }
    }
}
