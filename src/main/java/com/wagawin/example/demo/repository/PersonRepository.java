package com.wagawin.example.demo.repository;

import com.wagawin.example.demo.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long>
{
    /*@Query(value = "select count(p.id) as numberOrParents , count(c.id) as numberOfChildren from persons p join children c on p.id=c.person_id",
            nativeQuery = true)
    Object[] findPersonChildrenSummary();*/
}
