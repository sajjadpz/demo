package com.wagawin.example.demo.repository;

import com.wagawin.example.demo.model.Child;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sajjadpervaiz on 11/01/2018.
 */
@Repository
public interface ChildRepository extends JpaRepository<Child, Long>,JpaSpecificationExecutor<Child>
{
    @Query(value = "select p.person_name, m.name from persons p join children c on p.id=c.person_id join meals m on m.child_id=c.id where c.id=?1",
            nativeQuery = true)
    List<Object[]> findChildInfo(Long id);
}
