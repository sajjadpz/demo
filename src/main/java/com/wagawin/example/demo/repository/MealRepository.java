package com.wagawin.example.demo.repository;

import com.wagawin.example.demo.model.House;
import com.wagawin.example.demo.model.Meal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by sajjadpervaiz on 11/01/2018.
 */
@Repository
public interface MealRepository extends JpaRepository<Meal, Long>
{
}
