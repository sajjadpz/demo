package com.wagawin.example.demo.repository;

import com.wagawin.example.demo.model.Child;
import com.wagawin.example.demo.model.Child_;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sajjadpervaiz on 15/01/2018.
 *
 * Hibernate has limitation for polymorphich queries
 * Also JPQL has limited support for parametrized polymorphic queries, therfor using criteria builder API
 * along with Spring data JPA, this class mixed both approaches.
 */

@Component
public class ChildColorSpecifications{
    public static Specification<Child> withDynamicQuery(final Long id) {
        return new Specification<Child>() {
            @Override
            public Predicate toPredicate(Root<Child> child, CriteriaQuery<?> query, CriteriaBuilder builder) {
                if (id == null) {
                    throw new IllegalStateException("one parameter is needed");
                }
                List<Predicate> predicates = new ArrayList<>();
                if (id != null) {
                    predicates.add(builder.and(builder.equal(child.get(Child_.id) , id)));
                }
                Predicate[] predicatesArray = new Predicate[predicates.size()];
                return builder.and(predicates.toArray(predicatesArray));
            }
        };
    }
}
