package com.wagawin.example.demo.repository;

import com.wagawin.example.demo.model.House;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HouseRepository extends JpaRepository<House, Long>
{
    @Query(value = "select * from house h join persons p on p.id=h.person_id",
            nativeQuery = true)
    List<House> findHouseofPerson();
}
