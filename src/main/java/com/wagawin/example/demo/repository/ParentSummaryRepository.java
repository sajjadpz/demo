package com.wagawin.example.demo.repository;

import com.wagawin.example.demo.model.ParentSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ParentSummaryRepository extends JpaRepository<ParentSummary,Long>
{
    @Transactional
    @Modifying
    @Query(value = "UPDATE parent_summary ps set ps.amount_of_children =?1, ps.amount_of_persons=?2",nativeQuery = true)
    void updateSummary(Long amountOfChildren,Long amountOfPersons);

}
