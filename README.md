# My project's README

## DataBase

Please create database manually by the name of `wagawinworld`

##Tools/Technology stack

`Spring-boot with JPA / Hibernate`
`Maven`
`Intellij`

`DemoApplication` is entry point of application.


## Running Application

`mvn spring-boot:run`
